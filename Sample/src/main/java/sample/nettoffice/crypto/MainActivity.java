package sample.nettoffice.crypto;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import library.neetoffice.com.crypto.CryptoUtil;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = (TextView) findViewById(R.id.sample_text);
        CryptoUtil cryptoUtil = new CryptoUtil(this);
        String encode = cryptoUtil.encode("Hello AES 256 by C++");
        tv.setText(encode);
        tv.append("\n");
        tv.append(cryptoUtil.decode(encode));
    }
}

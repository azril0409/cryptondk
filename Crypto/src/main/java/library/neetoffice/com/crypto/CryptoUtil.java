package library.neetoffice.com.crypto;

import android.content.Context;
import android.util.Base64;

/**
 * Created by Deo-chainmeans on 2017/8/7.
 */

public class CryptoUtil {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private final Context context;

    public CryptoUtil(Context context) {
        this.context = context;
    }


    /**
     * AES加密
     *
     * @return
     */
    public String encode(String text) {
        return Base64.encodeToString(encode(context, text), Base64.DEFAULT);
    }

    /**
     * AES 解密
     *
     * @return
     */
    public String decode(String text) {
        return new String(decode(context, Base64.decode(text, Base64.DEFAULT)));
    }

    /**
     * AES加密
     *
     * @param context
     * @param str
     * @return
     */
    private native byte[] encode(Object context, String str);


    /**
     * AES 解密
     *
     * @param context
     * @param str
     * @return UNSIGNATURE ： sign not pass .
     */
    private native byte[] decode(Object context, byte[] str);
}

//
// Created by Deo-chainmeans on 2017/8/8.
//
// for Mac : keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
// for Windows : keytool -exportcert -alias androiddebugkey -keystore %HOMEPATH%\.android\debug.keystore | openssl sha1 -binary | openssl base64
static const char *keyHashes[2] = {"wp6p4L8qvu2s8QT44ltdVPIIock=",""};

static const char *packageName = "sample.nettoffice.crypto";

static const char *transformation = "AES/CBC/PKCS5Padding";

static const char *keyAlgorithm = "AES";

static const char *key = "RQVYFcL6nhBYiRpQzAGVrTpZ148deYss";

static const char *iv = "n0hDZkhkcyD0nnsn";
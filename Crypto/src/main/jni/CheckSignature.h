//
// Created by Deo-chainmeans on 2017/8/8.
//
#include <jni.h>

#ifndef CRYPTONDK_CHECKSIGNATURE_H
#define CRYPTONDK_CHECKSIGNATURE_H

jint checkSignature(JNIEnv *env, jobject context);

#endif //CRYPTONDK_CHECKSIGNATURE_H

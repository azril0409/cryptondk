#include <jni.h>
#include "Config.cpp"
#include "CheckSignature.h"


static const char *ENCRYPT_MODE = "ENCRYPT_MODE";
static const char *DECRYPT_MODE = "DECRYPT_MODE";


jbyteArray getBytes(JNIEnv *env, jstring string) {
    jclass stringClass = env->FindClass("java/lang/String");
    jmethodID string_getBytes = env->GetMethodID(stringClass, "getBytes", "(Ljava/lang/String;)[B");
    return (jbyteArray) env->CallObjectMethod(string, string_getBytes, env->NewStringUTF("UTF-8"));
}

jobject getCipher(JNIEnv *env) {
    //Cipher cipher = Cipher.getInstance(transformation);
    jclass cipherClass = env->FindClass("javax/crypto/Cipher");
    jmethodID cipher_getInstance = env->GetStaticMethodID(cipherClass, "getInstance","(Ljava/lang/String;)Ljavax/crypto/Cipher;");
    jobject cipher = env->CallStaticObjectMethod(cipherClass, cipher_getInstance,env->NewStringUTF(transformation));
    return cipher;
}

jobject getSecretKeySpec(JNIEnv *env) {
    //SecretKeySpec    myKey = new SecretKeySpec(key , keyAlgorithm);
    jclass keySpecClass = env->FindClass("javax/crypto/spec/SecretKeySpec");
    jmethodID keySpec_init = env->GetMethodID(keySpecClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray keyByteArray = getBytes(env, env->NewStringUTF(key));
    jobject secretKeySpec = env->NewObject(keySpecClass, keySpec_init, keyByteArray,env->NewStringUTF(keyAlgorithm));
    return secretKeySpec;
}

jobject getIvParameterSpec(JNIEnv *env) {
    //IvParameterSpec ivspec = new IvParameterSpec(initializationVector);
    jclass ivClass = env->FindClass("javax/crypto/spec/IvParameterSpec");
    jmethodID ivClass_init = env->GetMethodID(ivClass, "<init>", "([B)V");
    jbyteArray ivByteArray = getBytes(env, env->NewStringUTF(iv));
    jobject ivParameterSpec = env->NewObject(ivClass, ivClass_init, ivByteArray);
    return ivParameterSpec;
}

void init(JNIEnv *env, jobject cipher, const char *opmode, jobject secretKeySpec,jobject ivParameterSpec) {
    //cipher.init(Cipher.DECRYPT_MODE, myKey, ivspec);
    jclass cipherClass = env->GetObjectClass(cipher);
    jmethodID cipher_init = env->GetMethodID(cipherClass, "init","(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V");
    jfieldID mode = env->GetStaticFieldID(cipherClass, opmode, "I");
    jint field_dec = env->GetStaticIntField(cipherClass, mode);
    env->CallVoidMethod(cipher, cipher_init, field_dec, secretKeySpec, ivParameterSpec);
}

jbyteArray doFinal(JNIEnv *env, jobject cipher, jbyteArray byteArray){
    //return cipher.doFinal(text.getBytes());
    jclass cipherClass = env->GetObjectClass(cipher);
    jmethodID cipher_doFinal = env->GetMethodID(cipherClass, "doFinal", "([B)[B");
    return (jbyteArray) env->CallObjectMethod(cipher, cipher_doFinal, byteArray);
}

extern "C" JNIEXPORT jbyteArray JNICALL Java_library_neetoffice_com_crypto_CryptoUtil_encode(JNIEnv *env, jobject instance, jobject context, jstring text) {
    if(checkSignature(env,context)!=1){
        return env->NewByteArray(0);
    }
    jobject cipher = getCipher(env);
    jobject secretKeySpec = getSecretKeySpec(env);
    jobject ivParameterSpec = getIvParameterSpec(env);
    init(env, cipher, ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
    jbyteArray textByteArray = getBytes(env, text);
    return doFinal(env,cipher,textByteArray);
}

extern "C" JNIEXPORT jbyteArray JNICALL Java_library_neetoffice_com_crypto_CryptoUtil_decode(JNIEnv *env, jobject instance, jobject context, jbyteArray text) {
    if(checkSignature(env,context)!=1){
        return env->NewByteArray(0);
    }
    jobject cipher = getCipher(env);
    jobject secretKeySpec = getSecretKeySpec(env);
    jobject ivParameterSpec = getIvParameterSpec(env);
    init(env, cipher, DECRYPT_MODE, secretKeySpec, ivParameterSpec);
    return doFinal(env,cipher,text);
}
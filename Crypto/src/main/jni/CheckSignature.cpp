#include <jni.h>
#include <string.h>
#include "Config.cpp"

jobject getPackageManager(JNIEnv *env,jobject context) {
    // PackageManager packageManager = context.getPackageManager()
    jclass contextClass = env->GetObjectClass(context);
    jmethodID context_getPackageManager = env->GetMethodID(contextClass, "getPackageManager", "()Landroid/content/pm/PackageManager;");
    jobject packageManager = env->CallObjectMethod(context, context_getPackageManager);
    return packageManager;
}

jstring getPackageName(JNIEnv *env,jobject context){
    jclass contextClass = env->GetObjectClass(context);
    jmethodID context_getPackageName = env->GetMethodID(contextClass, "getPackageName", "()Ljava/lang/String;");
    return (jstring) env->CallObjectMethod(context, context_getPackageName);
}

jobject getPackageInfo(JNIEnv *env,jobject context,jobject packageManager){
    jstring packageName = getPackageName(env,context);
    jclass packageManagerClass = env->FindClass("android/content/pm/PackageManager");
    jmethodID packageManager_getPackageInfo = env->GetMethodID(packageManagerClass, "getPackageInfo", "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    jfieldID field = env->GetStaticFieldID(packageManagerClass, "GET_SIGNATURES", "I");
    jint flags = env->GetStaticIntField(packageManagerClass, field);
    return (jobject) env->CallObjectMethod(packageManager, packageManager_getPackageInfo,packageName, flags);
}

jobjectArray getSignatures(JNIEnv *env,jobject packageInfo){
    jclass packageInfoClass = env->GetObjectClass(packageInfo);
    jfieldID field_signatures = env->GetFieldID(packageInfoClass,"signatures", "[Landroid/content/pm/Signature;");
    return  (jobjectArray) env->GetObjectField(packageInfo, field_signatures);
}

jbyteArray toByteArrayBySignature(JNIEnv *env, jobject signature) {
    jclass signatureClass = env->GetObjectClass(signature);
    jmethodID signature_toByteArray = env->GetMethodID(signatureClass, "toByteArray", "()[B");
    return (jbyteArray) env->CallObjectMethod(signature, signature_toByteArray);
}

jobject getMessageDigest(JNIEnv *env){
    jclass messageDigestClass = env->FindClass("java/security/MessageDigest");
    jmethodID messageDigest_getInstance = env->GetStaticMethodID(messageDigestClass, "getInstance","(Ljava/lang/String;)Ljava/security/MessageDigest;");
    return env->CallStaticObjectMethod(messageDigestClass, messageDigest_getInstance,env->NewStringUTF("SHA"));
}

void updateByMessageDigest(JNIEnv *env, jobject messageDigest, jbyteArray signatureByteArray){
    jclass messageDigestClass = env->GetObjectClass(messageDigest);
    jmethodID messageDigest_update = env->GetMethodID(messageDigestClass, "update", "([B)V");
    env->CallVoidMethod(messageDigest, messageDigest_update, signatureByteArray);
}

jbyteArray digestByMessageDigest(JNIEnv *env, jobject messageDigest){
    jclass messageDigestClass = env->GetObjectClass(messageDigest);
    jmethodID messageDigest_toByteArray = env->GetMethodID(messageDigestClass, "digest", "()[B");
    return (jbyteArray) env->CallObjectMethod(messageDigest, messageDigest_toByteArray);
}

jstring base64EncodeToString(JNIEnv *env,jbyteArray byteArray){
    jclass base64Class = env->FindClass("android/util/Base64");
    jmethodID base64_encodeToString = env->GetStaticMethodID(base64Class, "encodeToString","([BI)Ljava/lang/String;");
    jfieldID field = env->GetStaticFieldID(base64Class, "DEFAULT", "I");
    jint flags = env->GetStaticIntField(base64Class, field);
    jstring string = (jstring) env->CallStaticObjectMethod(base64Class, base64_encodeToString, byteArray, flags);

    jclass stringClass = env->FindClass("java/lang/String");
    jmethodID string_length = env->GetMethodID(stringClass, "length", "()I");
    jint length = env->CallIntMethod(string,string_length);
    jmethodID string_substring = env->GetMethodID(stringClass, "substring", "(II)Ljava/lang/String;");
    return (jstring) env->CallObjectMethod(string, string_substring, 0, length - 1);

}


jint checkSignature(JNIEnv *env, jobject context) {
    jobject packageManager = getPackageManager(env, context);
    jobject packageInfo = getPackageInfo(env, context, packageManager);
   jstring _packageName = getPackageName(env,context);
    const char *package_name = env->GetStringUTFChars(_packageName, 0);
    if (strcmp(package_name, packageName) != 0) {
        return -1;
    }
    jobjectArray signatures =  getSignatures(env, packageInfo);
    jobject signature = env->GetObjectArrayElement(signatures, 0);
    jbyteArray signatureByteArray = toByteArrayBySignature(env, signature);
    jobject messageDigest = getMessageDigest(env);
    updateByMessageDigest(env, messageDigest, signatureByteArray);
    jbyteArray bytes = digestByMessageDigest(env, messageDigest);
    jstring _keyHash = base64EncodeToString(env,bytes);
    const char *key_hash = env->GetStringUTFChars(_keyHash, 0);
    int result = -2;
    int i=0;
    while(keyHashes[i] != NULL){
        const  char  *keyHash = keyHashes[i];
        if (strcmp(key_hash, keyHash) == 0) {
            result = 1;
            break;
        }
        i++;
    }
    return result;
}